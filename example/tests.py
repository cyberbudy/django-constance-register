from django.test import TestCase
from constance import config


class ConfTestCase(TestCase):
    def test_settings(self):
        self.assertIsInstance(config.BANNER, str)
